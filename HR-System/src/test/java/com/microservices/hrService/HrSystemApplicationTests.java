package com.microservices.hrService;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = { "spring.profiles.active: dev" })
class HrSystemApplicationTests {

	@Test
	void contextLoads() {
	}

}
