package com.microservices.hrService.repositories;

import com.microservices.hrService.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserName(String username);
}

