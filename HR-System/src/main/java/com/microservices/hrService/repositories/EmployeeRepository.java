package com.microservices.hrService.repositories;

import com.microservices.hrService.models.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee,Long> {
    Employee findByName(String name);
}
