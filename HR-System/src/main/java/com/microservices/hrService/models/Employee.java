package com.microservices.hrService.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="employees")
public class Employee {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    private long id;

    @Column(name = "user_name", nullable = false)
    private String name;

    @Column(nullable = false)
    private String department;

    @Column(nullable = false)
    private int age;

    @Column(nullable = false)
    private float salary;

    @Column(nullable = false)
    private Date date_of_birth;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt = new Date();
}
