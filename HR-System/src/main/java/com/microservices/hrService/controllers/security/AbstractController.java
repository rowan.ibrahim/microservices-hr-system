package com.microservices.hrService.controllers.security;

import com.microservices.hrService.models.UserPrincipal;
import org.springframework.security.core.Authentication;

public abstract class AbstractController {

    public UserPrincipal getUserDetails(Authentication authentication) {
        return (UserPrincipal) authentication.getPrincipal();
    }

}

