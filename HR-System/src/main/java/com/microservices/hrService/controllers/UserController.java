package com.microservices.hrService.controllers;

import com.microservices.hrService.controllers.security.AbstractController;
import com.microservices.hrService.models.User;
import com.microservices.hrService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;

import java.util.List;

@RestController
public class UserController extends AbstractController {
    @Autowired
    UserService userService;

    @GetMapping("/user")
    private List<User> getAllUsers(Authentication auth) {
        return userService.getAllUsers();
    }

    @GetMapping("/user/info")
    private User getUser(Authentication authentication) {
        User user = getUserDetails(authentication).getUser();
        return user;
    }

    @DeleteMapping("/user/{id}")
    private void deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
    }

    @PostMapping("/user")
    private User saveUser(@RequestBody User user) {
        userService.saveOrUpdate(user);
        return user;
    }
}
