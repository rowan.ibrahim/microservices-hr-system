package com.microservices.hrService.bean;

public class HrConfiguration {

    private int maximum;
    private int minimum;

    //no-argument constructor
    protected HrConfiguration() {
    }

    //generating getters
    public int getMaximum() {
        return maximum;
    }

    public int getMinimum() {
        return minimum;
    }

    //generating constructor using fields
    public HrConfiguration(int maximum, int minimum) {
        super();
        this.maximum = maximum;
        this.minimum = minimum;
    }
}

