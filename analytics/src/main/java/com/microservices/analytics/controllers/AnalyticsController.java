package com.microservices.analytics.controllers;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.json.JSONObject;

@Controller
public class AnalyticsController {
    @GetMapping("/analytics")
    public ResponseEntity list() throws JSONException {
//        JSONObject returnedObject = new JSONObject().put("name","Analytics Service");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
